#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "DDP_02_02.h"


bool	is_valid_jump(char *word, char *dico_word)
{
	int jumps = 0;

	while (*dico_word)
	{
		if (*dico_word != *word)
			jumps++;

		word++;
		dico_word++;
	}

	return (jumps == 1) ? (true)  : (false);
}


FILE 	*open_file(char *path, char *mode)
{
	FILE *fptr = fopen(path, mode);
	if (fptr == NULL)
	{
		fprintf(stderr, "Opening [%s] failed in function [%s]\n", path, __func__);
		perror("perror() report");
		exit(EXIT_FAILURE);
	}

	return (fptr);
}


void	jumping_words(char *word, FILE *dico)
{
	char dico_word[SIZE_WORD + 1] = {0};
	int index_dico = 0;

	rewind(dico);
	printf("%s :", word);
	fflush(stdout);

	while (index_dico < NB_WORDS_DICO)
	{
		get_word(dico_word, dico);
		fseek(dico, 1, SEEK_CUR);

		if ( is_valid_jump(word, dico_word) == true )
			printf(" %s", dico_word);

		index_dico++;
	}
}


void	get_word(char *word, FILE *stream)
{
	if ( fgets(word, SIZE_WORD + 1, stream) == NULL )
	{
		fprintf(stderr, "fgets() failed in function [%s]\n", __func__);
		perror("perror() report");
		exit(EXIT_FAILURE);
	}
}


int		main(void)
{
	char word[SIZE_WORD + 1] = {0};
	FILE *dico = NULL;

	dico = open_file("dico.txt", "r");

	printf("Envoyez une ligne vide (juste entree) pour quitter\n");
	while (1)
	{
		printf(">> ");
		fflush(stdout);
		get_word(word, stdin);
		if (word[0] == '\n')
			break;
		getchar();
		jumping_words(word, dico);
		printf("\n\n");
	}

	fclose(dico);

	return (EXIT_SUCCESS);
}