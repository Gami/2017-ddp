#ifndef DDP_02_02_HEADER
	#define DDP_02_02_HEADER


	#ifdef __cplusplus
		#error "T'es nouille toi, utilises un compilateur C pas C++ !"
	#endif


	#define SIZE_WORD 		5
	#define NB_WORDS_DICO	7823

	void	get_word(char *word, FILE *stream); // reads a word on [stream] and stores it in [word]
	void	jumping_words(char *word, FILE *dico); // prints the results
	FILE 	*open_file(char *path, char *mode); // opens a file, if fopen() fails, the function calls exit()
	bool	is_valid_jump(char *word, char *dico_word); // checks if [dico_word] is a "valid jump" for [word]


	

#endif