#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "DPP_02_01.h"


void	print_pattern(int pattern[], FILE *output_file)
{
	for (int i = 0 ; i < SIZE_PATTERN ; ++i)
	{
		switch (pattern[i])
		{
			case Inf : fputc('<', output_file); break;
			case Eq :  fputc('=', output_file); break;
			case Sup : fputc('>', output_file); break;
		}
	}
}


void	print_missing_patterns(s_pattern pattern, FILE *output_file)
{
	int index = 0;

	while (index < NB_PATTERNS)
	{
		if (pattern.nb_words[index] == 0)
		{
			print_pattern(pattern.all_patterns[index], output_file);
			fputc('\n', output_file);
		}

		index++;
	}
}


void 	print_less_commons_patterns(s_pattern pattern, FILE *output_file)
{
	int index = 0;
	while (index < NB_PATTERNS)
	{
		if (pattern.nb_words[index] > 0 && pattern.nb_words[index] <= NOT_COMMON_PATTERN)
		{
			print_pattern(pattern.all_patterns[index], output_file);
			fprintf(output_file, " (%d)", pattern.nb_words[index]);
			fputc('\n', output_file);
		}

		index++;
	}
}


void	print_most_commons_patterns(s_pattern pattern, FILE *output_file)
{
	int index = 0;

	while (index < NB_PATTERNS)
	{
		if (pattern.nb_words[index] >= COMMON_PATTERN)
		{
			print_pattern(pattern.all_patterns[index], output_file);
			fprintf(output_file, " (%d)", pattern.nb_words[index]);
			fputc('\n', output_file);
		}

		index++;
	}
}


void	print_all_patterns_with_numbers(s_pattern pattern, FILE *output_file)
{
	for (int index = 0 ; index < NB_PATTERNS ; ++index)
	{
		print_pattern(pattern.all_patterns[index], output_file);
		fprintf(output_file, "  correspond à %d mot(s)\n", pattern.nb_words[index]);
	}
}


void	print_results(s_pattern pattern)
{
	FILE *output_file = NULL;

	output_file = open_file("DPP_02_01_output.txt", "w");

	print_all_patterns_with_numbers(pattern, output_file);

	fprintf(output_file, "\n\nLes motifs les plus fréquents (au moins %d occurences) sont :\n", COMMON_PATTERN);
	print_most_commons_patterns(pattern, output_file);

	fprintf(output_file, "\n\nLes motifs les moins fréquents (%d occurences ou moins, mais pas 0) sont :\n", NOT_COMMON_PATTERN);
	print_less_commons_patterns(pattern, output_file);

	fprintf(output_file, "\n\nLes motifs manquants sont :\n");
	print_missing_patterns(pattern, output_file);

	fclose(output_file);
}


bool	compare_word_to_pattern(char *word, int pattern[])
{
	for (int i = 0 ; i < SIZE_PATTERN ; ++i)
	{
		if (word[i] < word[i + 1])
		{
			if (pattern[i] != Inf)
				return (false);
		}

		else if (word[i] == word[i + 1])
		{
			if (pattern[i] != Eq)
				return (false);
		}

		else if (word[i] > word[i + 1])
		{
			if (pattern[i] != Sup)
				return (false);
		}
	}

	return (true);
}


void	get_word(char *word, FILE *dico)
{
	if ( fgets(word, SIZE_WORD + 1, dico) == NULL )
	{
		fprintf(stderr, "fgets() failed in function [%s]\n", __func__);
		perror("perror() report");
		exit(EXIT_FAILURE);
	}
}


int		nb_word_for_this_pattern(int pattern[], FILE *dico)
{
	char current_word[SIZE_WORD + 1] = {0};
	int index_dico = 0;
	int index_pattern = 0;
	int number = 0;

	rewind(dico);

	while (index_dico < NB_WORDS_DICO)
	{
		get_word(current_word, dico);
		fseek(dico, 1, SEEK_CUR);

		if ( compare_word_to_pattern(current_word, pattern) == true )
			number++;

		index_dico++;
	}

	index_pattern++;
	
	return (number);
}


FILE 	*open_file(char *path, char *mode)
{
	FILE *fptr = fopen(path, mode);
	if (fptr == NULL)
	{
		fprintf(stderr, "Opening [%s] failed in function [%s]\n", path, __func__);
		perror("perror() report");
		exit(EXIT_FAILURE);
	}

	return (fptr);
}


void	nb_of_words_for_each_pattern(s_pattern *pattern)
{
	FILE *dico = NULL;
	int index = 0;

	dico = open_file("dico.txt", "r");

	while (index < NB_PATTERNS)
	{
		pattern->nb_words[index] = nb_word_for_this_pattern(pattern->all_patterns[index], dico);
		index++;
	}

	fclose(dico);
}


void	get__all_patterns(int all_patterns[NB_PATTERNS][SIZE_PATTERN])
{
	int index = 0;

	for (int i = 0 ; i <= 2 ; ++i)
		for (int j = 0 ; j <= 2 ; ++j)
			for (int k = 0 ; k <= 2 ; ++k)
				for (int l = 0 ; l <= 2 ; ++l)
				{
					all_patterns[index][0] = i;
					all_patterns[index][1] = j;
					all_patterns[index][2] = k;
					all_patterns[index][3] = l;
					index++;
				}
}


int		main(void)
{
	s_pattern pattern;

	get__all_patterns(pattern.all_patterns);
	nb_of_words_for_each_pattern(&pattern);
	print_results(pattern);

	return (EXIT_SUCCESS);
}