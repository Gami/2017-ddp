#ifndef DPP_02_01_HEADER
	#define DPP_02_01_HEADER

	#ifdef __cplusplus
		#error "T'es nouille toi, utilises un compilateur C pas C++ !"
	#endif

	#define NB_PATTERNS			81
	#define SIZE_PATTERN 		4
	#define SIZE_WORD 			5
	#define NB_WORDS_DICO		7823
	#define COMMON_PATTERN		500
	#define NOT_COMMON_PATTERN	100
	
	enum  /* Inferior Equal Superior */
	{
		Inf,
		Eq,
		Sup
	};

	struct s_pattern
	{
		int all_patterns[NB_PATTERNS][SIZE_PATTERN];
		int nb_words[NB_PATTERNS];
	};
	typedef struct s_pattern s_pattern;


	void	get__all_patterns(int all_patterns[NB_PATTERNS][SIZE_PATTERN]); /* stores in a 2D array all the different patterns */
	void	nb_of_words_for_each_pattern(s_pattern *pattern); /* stores in an array the number of occurrences for all the patterns */
	FILE 	*open_file(char *path, char *mode); /* opens a file with fopen(), if fopen() fails, open_file() calls exit() */
	int		nb_word_for_this_pattern(int pattern[], FILE *dico); /* get the number of occurrences for a pattern, in a file */
	void	get_word(char *word, FILE *dico); /* reads a word in a file, if fgets() fails, get_word() calls exit() */
	bool	compare_word_to_pattern(char *word, int pattern[]); /* returns true if [word] matches with [pattern] */
	void	print_results(s_pattern pattern); /* calls the functions that give the results */
	void	print_all_patterns_with_numbers(s_pattern pattern, FILE *output_file); /* print all the different patterns with their number of occurrences */
	void	print_most_commons_patterns(s_pattern pattern, FILE *output_file); /* please read the name of the function */
	void 	print_less_commons_patterns(s_pattern pattern, FILE *output_file); /* please read the name of the function */
	void	print_missing_patterns(s_pattern pattern, FILE *output_file); /* please read the name of the function */
	void	print_pattern(int pattern[], FILE *output_file); /* prints a pattern with its array */


#endif
